# `annextimelog` Changelog

## v0.3.1 - 22.12.2023 - Update PyPI urls

- PyPI url updates

## v0.3.0 - 22.12.2023 - Getting closer to _'just about usable'_

- hooking into git to provide functionality
- `atm su` can now list events in a specified time frame
- events now show a pretty-formatted duration
- `atm del` to remove an event
- `atm test` to run (ridiculously empty) test suite
- several fixes here and there

## v0.2.1 - 21.12.2023 - `ImportError` hotfix

- Forgot a `import json` 🙄

## v0.2.0 - 21.12.2023 - Basic functionality

- basic infrastructure and cli around a git annex repo
- basic time tracking with `atl track`

## v0.1.0 - 20.12.2023 - Outline

- Initial version with only the project outline available, no real code yet
